#' Get some details about a runner from its id
#' 
#' Performance index, sex, rank, ...
#' Runner id is found by using \link{itra_runners} beforehand.
#'
#' @param id (integer) runner id (found with \link{itra_runners})
#'
#' @return a \link[tibble]{tibble}
#'    One row per trail category + general rank : performance index, sex, 
#'    world/continent/country rank...
#' @export
#'
#' @examples
#' itra_runner_details(2704)
itra_runner_details <- function(id) {
  r <- httr::GET(glue::glue("https://itra.run/runner/{id}/results"))
  httr::warn_for_status(r)
  
  r %>% 
    httr::content(as = "text", encoding = "UTF-8") %>% 
    jsonlite::fromJSON() %>% 
    tibble::as_tibble()
}