<!-- README.md is generated from README.Rmd. Please edit that file -->
{itra}
======

<!-- badges: start -->
<!-- badges: end -->
[R](https://www.r-project.org/) package to access data on runners, races and results from the *International Trail Running Association* using <https://itra.run>.

**NOTE : as of 2021-08 and a website redesign, the ITRA endpoints have changed and this package is no longer functional. An update maybe be sooner or later...**

Installation
------------

You can install the current version of {itra} from Gitlab with: `remotes::install_gitlab("mdagr/itra")`.

Example
-------

``` r
library(itra)

itra_runners("jornet")
#> # A tibble: 5 x 15
#>   id_coureur     id  cote nom   prenom quartz team      M L     XL      XXL
#>        <int>  <int> <int> <chr> <chr>   <int> <chr> <int> <lgl> <lgl> <int>
#> 1       2704 2.70e3   950 JORN… Kilian     20 ""      898 NA    NA      896
#> 2     707322 7.07e5   538 RENU… Arturo      0 ""       NA NA    NA       NA
#> 3    2667939 2.67e6   520 PARE… Samuel      0 ""       NA NA    NA       NA
#> 4    2673933 2.67e6   468 PÉRE… Anna        0 ""       NA NA    NA       NA
#> 5    2673935 2.67e6   468 RIUS… Sergi       0 ""       NA NA    NA       NA
#> # … with 4 more variables: S <int>, XS <int>, XXS <lgl>, palmares <chr>

itra_runner_results(2704, "Kilian", "JORNET-BURGADA")
#> # A tibble: 84 x 28
#>     idca fullname dt    annee  etat id_course nb_pts nb_pts_tps nb_pts_mont
#>    <int> <chr>    <chr> <int> <int>     <int>  <int>      <int>       <int>
#>  1 51225 Sierre-… 2019…  2019     0      1501     NA         NA          NA
#>  2 52831 Pikes P… 2019…  2019     0      1515     NA         NA          NA
#>  3 39299 Annapur… 2019…  2019    -1     16481      2        320          13
#>  4 36979 Salomon… 2018…  2018     0      6535     NA         NA          NA
#>  5  3520 Ultra C… 2012…  2012     0       750     NA         NA          NA
#>  6  5288 Transvu… 2013…  2013     0       708     NA         NA          NA
#>  7 18719 Maratho… 2017…  2017    -1       432      3        350           9
#>  8 25155 Salomon… 2017…  2017     0      4109     NA         NA          NA
#>  9 17383 Utmb® 2… 2017…  2017    -1       142      6        420           8
#> 10  4232 Maratho… 2013…  2013     0       432     NA         NA          NA
#> # … with 74 more rows, and 19 more variables: dist_tot <chr>, deniv_tot <int>,
#> #   deniv_neg_tot <int>, canom <chr>, eanom <chr>, pays <chr>, place <int>,
#> #   rnksex <chr>, temps <chr>, cote <chr>, tcqiactive <int>, tcqiopen <chr>,
#> #   dt_end <chr>, cats <list>, inPeriod <int>, liked <int>, note <lgl>,
#> #   countconote <lgl>, nbres <int>

itra_runner_details(2704)
#> # A tibble: 8 x 14
#>   cote  sexe  is_prev dist    did period   pid km_max best_cote rang_mondial
#>   <chr> <chr>   <int> <chr> <int> <chr>  <int>  <int> <chr>            <int>
#> 1 950   H           0 GENE…     1 36m F…    71   1000 -                    2
#> 2 896   H           1 XXL       2 36m F…    71   1000 -                    5
#> 3 -     <NA>       NA XL        3 36m F…    71    210 -                   NA
#> 4 -     <NA>       NA L         4 36m F…    71    155 -                   NA
#> 5 898   H           1 M         5 36m F…    71    115 -                    5
#> 6 947   H           0 S         6 36m F…    71     75 -                    1
#> 7 888   H           1 XS        7 36m F…    71     45 -                    8
#> 8 -     <NA>       NA XXS       8 36m F…    71     25 -                   NA
#> # … with 4 more variables: rang_continent <int>, rang_pays <int>,
#> #   best_cote_h <chr>, best_cote_f <chr>
```

Vignette
--------

See [http://r.iresmi.net/2020/08/10/trail-running-is-it-worth-starting/]
